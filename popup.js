const resultElement = document.getElementById("result");

browser.tabs.executeScript({
    code: `window.getSelection().toString()`
}, function(selectionText) {
    resultElement.innerText(selectionText)
})